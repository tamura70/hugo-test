---
title: "Faq #139について"
date: 2019-11-15
tags: [ タグ194, タグ81 ]
---

## タグ

- タグ194
- タグ81

## テスト文

> Even the clearest and most perfect circumstantial evidence is likely to be at
fault, after all, and therefore ought to be received with great caution.  Take
the case of any pencil, sharpened by any woman; if you have witnesses, you will
find she did it with a knife; but if you take simply the aspect of the pencil,
you will say that she did it with her teeth.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

