---
title: "News #156について"
date: 2019-12-27
tags: [ タグ26, タグ21, タグ163, maintenance ]
---

## タグ

- タグ26
- タグ21
- タグ163
- maintenance

## テスト文

> In the space of one hundred and seventy-six years the Mississippi has
shortened itself two hundred and forty-two miles.  Therefore ... in the Old
Silurian Period the Mississippi River was upward of one million three hundred
thousand miles long ... seven hundred and forty-two years from now the
Mississippi will be only a mile and three-quarters long.  ... There is
something fascinating about science.  One gets such wholesome returns of
conjecture out of such a trifling investment of fact.
		-- Mark Twain

