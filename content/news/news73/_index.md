---
title: "News #73について"
date: 2019-11-25
tags: [ タグ88, trouble ]
---

## タグ

- タグ88
- trouble

## テスト文

> Whoever has lived long enough to find out what life is, knows how deep a debt
of gratitude we owe to Adam, the first great benefactor of our race.  He
brought death into the world.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

