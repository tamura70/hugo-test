---
title: "News #163について"
date: 2019-11-22
tags: [ タグ126, タグ110, maintenance ]
---

## タグ

- タグ126
- タグ110
- maintenance

## テスト文

> Behold, the fool saith, "Put not all thine eggs in the one basket"--which is
but a manner of saying, "Scatter your money and your attention;" but the wise
man saith, "Put all your eggs in the one basket and--WATCH THAT BASKET."
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

